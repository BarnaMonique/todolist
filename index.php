<?php
$server_name="localhost";
$username="root";
$password="";
$dbname="todolist";

$connection = mysqli_connect($server_name,$username,$password,$dbname);
/* check connection */
if (mysqli_connect_errno()) { // ez a fv. visszaadja a legutolsó lekérdezés hibakódját. a 0 azt jelenti, h nem volt hiba. A 0 false lesz, minden más pedig true.
    printf("Connect failed: %s\n", mysqli_connect_error()); // ez a fv. visszaadja a legutolsó hiba szövegét.
    exit(); // megszünteti a php futását.
}
?>

<!DOCTYPE html>
<html lang="HU">
<head>
	<title>Todolist</title>
	<meta charset="UTF-8">
	<style>
		body {
			background: #b0e5b7;
		}
		h1{
			font-size: 30px;
			color: #FFFFFF;
			text-align: center;
		}
	</style>
</head>
<body>
	<h1>Teendők listája!</h1>
	<ul>
		<?php
		$query_string = "SELECT * FROM todo";
		$result = $connection->query($query_string); // a query függvény lekérdezi az adatbázisból a todo-kat, és visszaadja a lekérdezés eredményét
		while($row = mysqli_fetch_array($result)) // a mysqli_fetch_array() fv. egy sornak a tömbjével, vagy NULL-al tér vissza. A NULL false-á, a tömb pedig true-vá.
		{
			echo "<li>".$row["title"]."</li>";
		}
		?>
	</ul>
</body>
</html>